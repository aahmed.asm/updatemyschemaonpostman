# UpdateMySchemaOnPostman

Enabling users/teams/organisations to sync their API Schemas on Postman after any update to their API Schema on their Gitlab instance

# Pre-requisites
Ensure you have a Gitlab instance setup
Ensure NPM and Node are installed

# Setup
* Clone or download the repository from: https://gitlab.com/aahmed.asm/updatemyschemaonpostman.git
* Take a look at: Collection Variables on how we can update the collection variables
* Take a look at the Postman API documentation for API to learn about the operations currently available using the Postman API.
* Update the index.js file appropriately with the directory where the Schema file resides
* Update the config.yml(placeholder) files with the appropriate information
* Push the code to your Gitlab instance where you will be updating the JSON Schema file
* Execute the command 'make' from the CLI.