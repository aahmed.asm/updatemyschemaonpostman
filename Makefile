BIN = node_modules/.bin
.PHONY: bootstrap lint start test
bootstrap:
	npm install
	npm install config-yml --save
	node index.js
lint:
	$(BIN)/standard